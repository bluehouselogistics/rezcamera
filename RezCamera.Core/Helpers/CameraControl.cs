﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RezCamera.Core.Services;

namespace RezCamera.Core.Helpers
{
    public class CameraControl
    {
        public struct Direction
        {
            public const string Up = "1";
            public const string Down = "2";
            public const string Left = "3";
            public const string Right = "4";
        }

        public struct Location
        {
            public const string Band = "0";
            public const string Table = "1";
            public const string Stage = "2";
            public const string Reader = "3";
        }

        private HttpDataService _service;

        public CameraControl(string username, string password, string url)
        {
            var httpAuth = new HttpAuth(username, password);
            _service = new HttpDataService(httpAuth, url);
        }

        public async Task<bool> moveToLocation(string locationID)
        {
            var parameters = new Dictionary<String, String>
            {
                { "flag", "4" },
                { "existFlag", "1" },
                { "presetNum", locationID },
            };

            return await _service.PostForm<bool>("/form/presetSet", parameters);
        }

        public async Task<bool> toggleNightMode(bool isOn)
        {
            var irMode = isOn ? "2" : "3";
            var irEnable = isOn ? "1" : "0";

            var parameters = new Dictionary<String, String>
            {
                { "IRmode", irMode },
                { "c2bwthr", "20" },
                { "bw2cthr", "70" },
                { "IRenable", irEnable },
                { "IRdelay", "3" }
            };

            return await _service.PostForm<bool>("/form/IRset", parameters);
        }

        public async Task<bool> zoom(bool isZoomIn)
        {
            var command = isZoomIn ? "13" : "14";

            var response = await Command(command);
            
            await Task.Delay(200);
            
            _ = await stop();

            return response;
        }

        public async Task<bool> move(string direction)
        {
            var response = await Command(direction);

            await Task.Delay(200);

            _ = await stop();

            return response;
        }

        private async Task<bool> Command(string command)
        {
            var parameters = new Dictionary<String, String>
            {
                { "command", command },
                { "panSpeed", "1" },
                { "zoomSpeed", "1" },
                { "tiltSpeed", "1" },
                { "focusSpeed", "6" },
                { "ZFSpeed", "1" },
                { "PTSpeed", "1" }
            };

            return await _service.PostForm<bool>("/form/setPTZCfg", parameters); ;
        }

        public async Task<bool> stop()
        {
            return await Command("0");
        }
    }
}
