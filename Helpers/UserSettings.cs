﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Windows.ApplicationModel.Resources;
using Windows.Storage;

namespace RezCamera.Helpers
{
    public class UserSettings
    {
        public string InternalIPAddress;
        public string ExternalURL;
        public string Username;
        private string PasswordEncrypt;
        public bool isInternalAddressUsed;

        public string Password
        {
            get
            {
                if (String.IsNullOrEmpty(PasswordEncrypt))
                {
                    return PasswordEncrypt;
                }

                return encryptionHelper.DecryptString(PasswordEncrypt, _passphrase);
            }

            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    PasswordEncrypt = null;
                } else
                {
                    PasswordEncrypt = encryptionHelper.EncryptString(value, _passphrase);
                }
            }
        }

        private static string _filename = "settings.json";
        private static string _passphrase = "RezCamera";
        private static EncryptionHelper encryptionHelper = new EncryptionHelper();

        public string CameraURL
        {
            get
            {
                if (isInternalAddressUsed)
                {
                    return InternalIPAddress;
                } else
                {
                    return ExternalURL;
                }
            }
        }

        public static async Task<UserSettings> Load()
        {
            var fileRef = ApplicationData.Current.LocalFolder;

            try
            {
                var file = await fileRef.GetFileAsync(_filename);
                var json = await FileIO.ReadTextAsync(file);
                
                return JsonConvert.DeserializeObject<UserSettings>(json);
            }
            catch
            {
                var settings = new UserSettings
                {
                    InternalIPAddress = null,
                    ExternalURL = null,
                    isInternalAddressUsed = true
                };
                var file = await settings.Save();
                var json = await FileIO.ReadTextAsync(file);
                
                return JsonConvert.DeserializeObject<UserSettings>(json);
            }
        }

        public async Task<StorageFile> Save()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);

            Console.WriteLine(json);

            var folder = ApplicationData.Current.LocalFolder;
            var file = await folder.CreateFileAsync(_filename, CreationCollisionOption.ReplaceExisting);

            await FileIO.WriteTextAsync(file, json);

            return file;
        }
    }

    public class EncryptionHelper
    {
        private Aes aesEncryptor;

        public EncryptionHelper()
        {
        }

        private void BuildAesEncryptor(string key)
        {
            aesEncryptor = Aes.Create();
            var pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            aesEncryptor.Key = pdb.GetBytes(32);
            aesEncryptor.IV = pdb.GetBytes(16);
        }

        public string EncryptString(string clearText, string key)
        {
            BuildAesEncryptor(key);
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, aesEncryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                }
                var encryptedText = Convert.ToBase64String(ms.ToArray());
                return encryptedText;
            }
        }

        public string DecryptString(string cipherText, string key)
        {
            BuildAesEncryptor(key);
            cipherText = cipherText.Replace(" ", "+");
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, aesEncryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                }
                var clearText = Encoding.Unicode.GetString(ms.ToArray());
                return clearText;
            }
        }
    }
}
