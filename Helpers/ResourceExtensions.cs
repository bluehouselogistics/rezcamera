﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using Windows.ApplicationModel.Resources;
using Windows.Storage;
using System.Threading.Tasks;

namespace RezCamera.Helpers
{
    internal static class ResourceExtensions
    {
        private static ResourceLoader _resLoader = new ResourceLoader();

        public static string GetLocalized(this string resourceKey)
        {
            return _resLoader.GetString(resourceKey);
        }
    }
}
