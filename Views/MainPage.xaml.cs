﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RezCamera.Core.Helpers;
using RezCamera.Helpers;
using System.Collections.Generic;

using Windows.UI.Xaml.Controls;

namespace RezCamera.Views
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private UserSettings _settings;
        private CameraControl _cameraControl;

        public MainPage()
        {
            InitializeComponent();
            setup();
        }

        private async void setup()
        {
            _settings = await UserSettings.Load();
            _cameraControl = new CameraControl(_settings.Username, _settings.Password, _settings.CameraURL);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void bandButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.moveToLocation(CameraControl.Location.Band);
        }

        private void tableButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.moveToLocation(CameraControl.Location.Table);
        }

        private void readerButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.moveToLocation(CameraControl.Location.Reader);
        }

        private void stageButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.moveToLocation(CameraControl.Location.Stage);
        }

        private void dayModeButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.toggleNightMode(false);
        }

        private void nightModeButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.toggleNightMode(true);
        }

        private void zoomInButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.zoom(true);
        }

        private void zoomOutButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.zoom(false);
        }

        private void moveDownButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.move(CameraControl.Direction.Down);
        }

        private void moveLeftButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.move(CameraControl.Direction.Left);
        }

        private void moveRightButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.move(CameraControl.Direction.Right);
        }

        private void moveUpButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _ = _cameraControl.move(CameraControl.Direction.Up);
        }
    }
}
