﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using RezCamera.Helpers;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace RezCamera.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {

        private UserSettings settings;

        public SettingsPage()
        {
            this.InitializeComponent();

            loadSettings();
        }

        private void internalIPSetButton_Click(object sender, RoutedEventArgs e)
        {
            settings.InternalIPAddress = internalIPTextBox.Text;

            saveSettings();
        }

        private void externalIPSetButton_Click(object sender, RoutedEventArgs e)
        {
            settings.ExternalURL = externalIPTextBox.Text;

            saveSettings();
        }

        private void useInternalIPButton_Checked(object sender, RoutedEventArgs e)
        {
            settings.isInternalAddressUsed = true;

            saveSettings();
        }

        private void useExternalIPButton_Checked(object sender, RoutedEventArgs e)
        {
            settings.isInternalAddressUsed = false;

            saveSettings();
        }

        private void credentialsSetButton_Click(object sender, RoutedEventArgs e)
        {
            settings.Username = usernameTextBox.Text;
            settings.Password = passwordBox.Password;

            saveSettings();
        }

        private async void loadSettings()
        {
            settings = await UserSettings.Load();

            if (!String.IsNullOrEmpty(settings.InternalIPAddress))
            {
                internalIPTextBox.Text = settings.InternalIPAddress;
            }

            if (!String.IsNullOrEmpty(settings.ExternalURL))
            {
                externalIPTextBox.Text = settings.ExternalURL;
            }

            if (!String.IsNullOrEmpty(settings.Username))
            {
                usernameTextBox.Text = settings.Username;
            }

            if (!String.IsNullOrEmpty(settings.Password))
            {
                passwordBox.Password = settings.Password;
            }

            if (settings.isInternalAddressUsed)
            {
                useInternalIPButton.IsChecked = true;
                useExternalIPButton.IsChecked = false;
            }
            else
            {
                useInternalIPButton.IsChecked = false;
                useExternalIPButton.IsChecked = true;
            }
        }

        private async void saveSettings()
        {
            await settings.Save();
        }

    }
}
